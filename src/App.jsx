import React, { Component } from "react";
import "./App.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import home from "./component/signin";
import Login from "./component/Login";

import {} from "react-router-dom";
function App() {
  return (
    <>
      <React.Fragment>
        <Switch>
          <Route exact path="/" component={home} />
          <Route exact path="/login" component={Login} />
        </Switch>
      </React.Fragment>
    </>
  );
}

export default App;
