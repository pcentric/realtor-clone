import React from "react";
import "./signin.css";

import { Icon, Button, Card } from "semantic-ui-react";
import GoogleMapReact from "google-map-react";

const AnyReactComponent = ({ text }) => <div>{text}</div>;

class signin extends React.Component {
  static defaultProps = {
    center: {
      lat: 59.95,
      lng: 30.33,
    },
    zoom: 11,
  };
  render() {
    return (
      <div>
        <nav class="navbar bg-test">
          <Icon name="left arrow" className="icon-test" />
          <a class="navbar-brand lg-test" href="#">
            LOGO
          </a>
          <Icon name="align center" className="icon-test" />
        </nav>
        <div className="map-test" >
          <Card className="card-test"  header='Bathurst Street Toronto, ON, CANADA' />
          <GoogleMapReact
            defaultCenter={this.props.center}
            defaultZoom={this.props.zoom}
          >
            <AnyReactComponent
              lat={59.955413}
              lng={30.337844}
              text="My Marker"
            />
          <Button onClick={()=>window.location='/login'} className="btn-test" content="POST YOUR AD" primary />
          </GoogleMapReact>
        </div>
      </div>
    );
  }
}

export default signin;
