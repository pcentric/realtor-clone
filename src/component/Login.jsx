import React from "react";
import "./Login.css";
import { Grid, Form, Card, Icon, Image } from "semantic-ui-react";

class Login extends React.Component {
  render() {
    return (
      <>
        <nav class="navbar bg-test">
          <Icon name="left arrow" className="icon-test" />
          <a class="navbar-brand lg-test" href="#">
            LOGO
          </a>
          <Icon name="align center" className="icon-test" />
        </nav>
        <div className="login-box container">
          <a>
            <Icon name="file image outline" className="icon-test1" />
            <p className="para-test">Add a Photo</p>
          </a>
        </div>
        <div className="container form-test">
          <form>
            <div class="form-group">
              <label for="formGroupExampleInput">Property Address</label>
              <input
                type="text"
                class="form-control"
                id="formGroupExampleInput"
                placeholder="Canada Street 565"
              />
            </div>
            <div class="form-group">
              <label for="formGroupExampleInput2">Property Title</label>
              <input
                type="text"
                class="form-control"
                id="formGroupExampleInput2"
                placeholder="Your Property Title"
              />
            </div>
            <div class="form-group">
              <label for="exampleFormControlTextarea1">
                Describe more about your property
              </label>
              <textarea
                class="form-control"
                id="exampleFormControlTextarea1"
                rows="5"
              ></textarea>
            </div>
          </form>
        </div>
      </>
    );
  }
}

export default Login;
